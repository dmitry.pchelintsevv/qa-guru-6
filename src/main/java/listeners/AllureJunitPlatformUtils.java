package listeners;

import org.junit.platform.engine.TestSource;
import org.junit.platform.engine.support.descriptor.ClassSource;
import org.junit.platform.engine.support.descriptor.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.stream.Stream;

public class AllureJunitPlatformUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllureJunitPlatformUtils.class);

    private AllureJunitPlatformUtils() {
        throw new IllegalStateException("do not instance");
    }

    public static Optional<String> getFullName(TestSource source) {
        if (source instanceof MethodSource) {
            MethodSource ms = (MethodSource)source;
            return Optional.of(String.format("%s.%s", ms.getClassName(), ms.getMethodName()));
        } else if (source instanceof ClassSource) {
            ClassSource cs = (ClassSource)source;
            return Optional.ofNullable(cs.getClassName());
        } else {
            return Optional.empty();
        }
    }

    public static Optional<Class<?>> getTestClass(TestSource source) {
        if (source instanceof MethodSource) {
            return getTestClass(((MethodSource)source).getClassName());
        } else {
            return source instanceof ClassSource ? Optional.of(((ClassSource)source).getJavaClass()) : Optional.empty();
        }
    }

    public static Optional<Class<?>> getTestClass(String className) {
        try {
            return Optional.of(Class.forName(className));
        } catch (ClassNotFoundException var2) {
            LOGGER.trace("Could not get test class from test source {}", className, var2);
            return Optional.empty();
        }
    }

    public static Optional<Method> getTestMethod(TestSource source) {
        return source instanceof MethodSource ? getTestMethod((MethodSource)source) : Optional.empty();
    }

    public static Optional<Method> getTestMethod(MethodSource source) {
        try {
            Class<?> aClass = Class.forName(source.getClassName());
            return Stream.of(aClass.getDeclaredMethods()).filter((method) -> {
                return MethodSource.from(method).equals(source);
            }).findAny();
        } catch (ClassNotFoundException var2) {
            LOGGER.trace("Could not get test method from method source {}", source, var2);
            return Optional.empty();
        }
    }
}
