package guru.qa.tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.common.collect.ImmutableMap;
import io.qameta.allure.selenide.AllureSelenide;
import listeners.Change;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;

@ExtendWith(BrowserStackNamingExtension.class)
public class BaseTest {

    @BeforeAll
    static void setUp() throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        String config_file = System.getProperty("bs", "local_desktop");
        String env_config_file = System.getProperty("env", "integration");

        JSONObject config = (JSONObject) parser.parse(
                new FileReader(String.format("src/test/resources/bs_configurations/%s.json", config_file)));
        JSONObject env_config = (JSONObject) parser.parse(
                new FileReader(String.format("src/test/resources/env_configurations/%s.json", env_config_file)));
        DesiredCapabilities capabilities = new DesiredCapabilities();

        Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
        for (Map.Entry<String, String> stringStringEntry : commonCapabilities.entrySet()) {
            Map.Entry<String, String> pair = stringStringEntry;
            if (capabilities.getCapability(pair.getKey()) == null) {
                capabilities.setCapability(pair.getKey(), pair.getValue());
            }
        }
        try {
            Configuration.remote = (String) config.get("remote_url");
        } catch (NullPointerException e) {
            Configuration.remote = null;
        }
        Configuration.baseUrl = (String) env_config.get("base_url");
        Configuration.pageLoadTimeout = 15000;
        Configuration.startMaximized = (Boolean) config.get("maximize");
        Configuration.browserSize = commonCapabilities.get("resolution");
        Configuration.browserCapabilities = capabilities;
        SelenideLogger.addListener("allure", new AllureSelenide());

        allureEnvironmentWriter(
                ImmutableMap.<String, String>builder()
                        .put("Browser", capabilities.getCapability("browser").toString())
                        .put("OS.Version", capabilities.getCapability("os_version").toString())
                        .put("OS", capabilities.getCapability("os").toString())
                        .build(), System.getProperty("user.dir")
                        + "/build/allure-results/");
    }
}
