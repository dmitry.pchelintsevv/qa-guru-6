package guru.qa.tests;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Allure;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class BrowserStackNamingExtension implements AfterTestExecutionCallback,
        BeforeTestExecutionCallback  {
    @Override
    public void afterTestExecution(ExtensionContext context) {
        if(System.getProperty("bs") != null && !System.getProperty("bs").equals("local_desktop")){
            if(context.getExecutionException().isPresent()) {
                Selenide.executeJavaScript(String.format("browserstack_executor: {\"action\": " +
                        "\"setSessionStatus\", \"arguments\": {\"status\":\"failed\"," +
                        " \"reason\": \"%s %s Integration Tests FAILED!\"}}", System.getProperty("name").toUpperCase(), context.getParent().get().getTags().toArray()[0]));
            }
            else {
                Selenide.executeJavaScript(String.format("browserstack_executor: {\"action\":" +
                        " \"setSessionStatus\", \"arguments\": {\"status\": \"passed\", " +
                        "\"reason\": \"%s %s Integration Tests passed!\"}}", System.getProperty("name").toUpperCase(), context.getParent().get().getTags().toArray()[0]));
            }
        }
    }

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        String taskName = System.getProperty("name");
        Allure.feature(taskName);
        Allure.getLifecycle().updateTestCase(testResult ->
                testResult.setName(System.getProperty("bs", "local_" +
                        taskName) + " " +
                        taskName.toUpperCase() + " " +testResult.getName()));
    }

}
