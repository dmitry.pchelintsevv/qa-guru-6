package guru.qa.tests;

import io.qameta.allure.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.DynamicTest.dynamicTest;

@Tag("AboutTheHotel")
public class GitHubTest extends BaseTest{

    private static final String REPOSITORY_NAME = "eroshenkoam/allure-example";
    private static final String ISSUE_NUMBER = "68";

    private final GitHubSteps gitHubSteps = new GitHubSteps();

//    @Test
//    @Tag("desktop")
//    @Tag("mobile")
//    @Feature("Issues")
//    @Story("Поиск по Issue")
//    @Owner("dpchelintsevv")
//    @Severity(SeverityLevel.BLOCKER)
//    @Description(
//            "Тест проверяет наличие задачи "+ ISSUE_NUMBER +
//                    " в репозитории " + REPOSITORY_NAME
//    )
//    @TestFactory
//     Collection<DynamicTest> dynamicTestsFromCollection() {
//        return Arrays.asList(
//                dynamicTest("1st dynamic test " + System.getProperty("name"),
//                        () -> Assertions.assertTrue(true)),
//                dynamicTest("2nd dynamic test " + System.getProperty("name"),
//                        () -> Assertions.assertTrue(false))
//        );
//    }
}
